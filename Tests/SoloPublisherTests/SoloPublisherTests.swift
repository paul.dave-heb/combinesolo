import XCTest
import Combine
@testable import SoloPublisher
import EntwineTest

final class SoloPublisherTests: XCTestCase {
    var cancelBag = Set<AnyCancellable>()

    func testSubscription() {
        let scheduler = TestScheduler()
        let solo: AnySolo<String, Never> = AnySolo<String, Never>.create { promise in
            promise(.success("hello"))
        }

        let observer = scheduler.createTestableSubscriber(String.self, Never.self)
        scheduler.schedule(after: 10) {
            solo.asPublisher().subscribe(observer)
        }
        scheduler.resume()

        let events = observer.recordedOutput
        XCTAssertEqual(events, [
            (10, .subscription),
            (10, .input("hello")),
            (10, .completion(.finished))
        ])
    }

    func testSink() {
        let completionExpectation = expectation(description: "solo did complete")

        let solo: AnySolo<String, Never> = AnySolo<String, Never>.create { promise in
            promise(.success("hello"))
        }
        solo.sink(onSuccess: {
            XCTAssertEqual($0, "hello")
            completionExpectation.fulfill()
        }, onFailure: { _ in XCTFail("this should not fail" )}).store(in: &cancelBag)

        wait(for: [completionExpectation], timeout: 0.2)
    }
}
