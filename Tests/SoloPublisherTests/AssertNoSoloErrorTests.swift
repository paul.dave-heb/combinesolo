//
//  AssertNoSoloErrorTests.swift
//  
//
//  Created by Paul,David on 2/11/21.
//

import XCTest
import Combine
@testable import SoloPublisher
import EntwineTest

private struct TestError: Error, Equatable {}

final class AssertNoSoloErrorTests: XCTestCase {
    var scheduler: TestScheduler!

    override func setUp() {
        super.setUp()
        scheduler = TestScheduler()
    }

    override func tearDown() {
        super.tearDown()
        soloAssertionFailures.removeAll()
    }

    func testEmptyStream() {
        let empty = Empty<String, Never>(completeImmediately: true)
        let observer = scheduler.createTestableSubscriber(String.self, Never.self)

        empty.asSolo().assertNoSoloError().asPublisher().subscribe(observer)
        scheduler.resume()

        // An assertion was triggered:
        XCTAssertEqual(soloAssertionFailures.count, 1)

        // In testing or production environments, instead of stopping execution, the stream simply never completes:
        XCTAssertEqual(observer.recordedOutput, [
            (0, .subscription),
        ])
    }

    func testMultipleElements() {
        let observer = scheduler.createTestableSubscriber(String.self, Never.self)
        let subject = PassthroughSubject<String, Never>()
        subject.asSolo().assertNoSoloError().asPublisher().subscribe(observer)

        scheduler.schedule(after: 10) {
            subject.send("one")
        }
        scheduler.schedule(after: 20) {
            subject.send("two")
        }

        scheduler.resume()

        // An assertion was triggered:
        XCTAssertEqual(soloAssertionFailures.count, 1)

        // In testing or production environments, instead of stopping execution, the stream simply never completes:
        XCTAssertEqual(observer.recordedOutput, [
            (0, .subscription)
        ])
    }

    func testUpstreamError() {
        let publisher = Fail<String, TestError>(error: TestError())
        let observer = scheduler.createTestableSubscriber(String.self, TestError.self)
        publisher.asSolo().assertNoSoloError().asPublisher().subscribe(observer)

        scheduler.resume()

        XCTAssertEqual(soloAssertionFailures.count, 0)
        XCTAssertEqual(observer.recordedOutput, [
            (0, .subscription),
            (0, .completion(.failure(TestError())))
        ])
    }
}
