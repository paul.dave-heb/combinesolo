//
//  AsSoloTests.swift
//  
//
//  Created by Paul,David on 2/11/21.
//

import XCTest
import Combine
@testable import SoloPublisher
import EntwineTest

final class AsSoloTests: XCTestCase {
    func testAsSolo() {
        let scheduler = TestScheduler()
        let publisher = Just("hello")
        let observer = scheduler.createTestableSubscriber(String.self, SoloError<Never>.self)
        publisher.asSolo().asPublisher().subscribe(observer)

        scheduler.resume()
        XCTAssertEqual(observer.recordedOutput, [
            (0, .subscription),
            (0, .input("hello")),
            (0, .completion(.finished))
        ])
    }

    func testAsSolo_tooManyElements() {
        let scheduler = TestScheduler()
        let subject = PassthroughSubject<String, Never>()
        let observer = scheduler.createTestableSubscriber(String.self, SoloError<Never>.self)
        subject.asSolo().asPublisher().subscribe(observer)

        scheduler.schedule(after: 10) {
            subject.send("hello")
        }
        scheduler.schedule(after: 20) {
            subject.send("world")
        }
        scheduler.resume()

        XCTAssertEqual(observer.recordedOutput, [
            (0, .subscription),
            (20, .completion(.failure(.tooManyElements)))
        ])
    }

    func testAsSolo_noElements() {
        let scheduler = TestScheduler()
        let publisher = Empty<String, Never>(completeImmediately: true)
        let observer = scheduler.createTestableSubscriber(String.self, SoloError<Never>.self)
        publisher.asSolo().asPublisher().subscribe(observer)

        scheduler.resume()
        XCTAssertEqual(observer.recordedOutput, [
            (0, .subscription),
            (0, .completion(.failure(.noElements)))
        ])
    }
}
