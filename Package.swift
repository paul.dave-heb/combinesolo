// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SoloPublisher",
    platforms: [.iOS(.v14), .macOS("10.15")],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SoloPublisher",
            targets: ["SoloPublisher"]),
    ],
    dependencies: [
        .package(url: "https://github.com/tcldr/Entwine.git", from: "0.9.1")
    ],
    targets: [
        .target(
            name: "SoloPublisher",
            dependencies: []),
        .testTarget(
            name: "SoloPublisherTests",
            dependencies: ["SoloPublisher", .product(name: "EntwineTest", package: "Entwine")]),
    ]
)
