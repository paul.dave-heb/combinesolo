//
//  Solo.swift
//
//
//  Created by Paul,David on 2/10/21.
//
import Combine

/// A type erased Solo.
public typealias AnySolo<O, F: Error> = Solo<AnyPublisher<O, F>>

/// A Solo is a publisher containing exactly 1 element.
/// Solos can either finish with a single element, or fail.
/// They cannot finish successfully without emitting any elements, nor can they emit more than one element.
public struct Solo<P: Publisher> {
    internal let publisher: P
}

public extension Solo {
    /// Creates an Solo sequence from a specified subscribe method implementation.
    static func create<Output, Failure>(_ factory: @escaping (Future<Output, Failure>.Promise) -> Void) -> Solo<AnyPublisher<Output, Failure>> {
        let publisher = Deferred(createPublisher: {
            Future(factory)
        }).eraseToAnyPublisher()
        return Solo<AnyPublisher<Output, Failure>>(publisher: publisher)
    }

    /// A publisher that emits an output to each subscriber just once, and then finishes.
    static func just<Output>(input: Output) -> Solo<Just<Output>> {
        Solo<Just<Output>>(publisher: Just(input))
    }

    /// Creates a publisher that never emits any values or completes.
    static func never<Output>() -> Solo<Empty<Output, Never>> {
        Solo<Empty<Output, Never>>(publisher: Empty(completeImmediately: false))
    }

    /// Creates a publisher that immediately terminates with the specified error.
    static func fail<Output, Failure: Error>(error: Failure) -> Solo<Fail<Output, Failure>> {
        Solo<Fail>(publisher: Fail(error: error))
    }

    func asPublisher() -> P {
        publisher
    }
}


