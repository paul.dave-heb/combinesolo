//
//  Solo+Sink.swift
//  
//
//  Created by Paul,David on 2/10/21.
//

import Combine

public extension Solo {
    /// Attaches a subscriber with closure-based behavior.
    func sink(onSuccess: @escaping (P.Output) -> Void, onFailure: @escaping (P.Failure) -> Void) -> AnyCancellable {
        var stopped = false
        return publisher.sink(receiveCompletion: {
            if stopped { return }
            stopped = true

            switch $0 {
            case .failure(let error): onFailure(error)
            case .finished: soloAssertionFailure("a value was not received before completion!")
            }
        }, receiveValue: {
            if stopped { return }
            stopped = true

            onSuccess($0)
        })
    }
}
