//
//  Solo+Operators.swift
//  
//
//  Created by Paul,David on 2/10/21.
//

import Combine

public extension Solo {
    /// Wraps this publisher with a type eraser.
    func eraseToAnySolo() -> AnySolo<P.Output, P.Failure> {
        Solo<AnyPublisher<P.Output, P.Failure>>(publisher: publisher.eraseToAnyPublisher())
    }

    /// Projects each element of an observable sequence into a new form.
    func map<Result>(_ transform: @escaping (P.Output) -> Result) -> Solo<Publishers.Map<P, Result>> {
        let downstream: Publishers.Map<P, Result> = publisher.map(transform)
        return Solo<Publishers.Map<P, Result>>(publisher: downstream)
    }

    /// Projects each element of an observable sequence to an observable sequence and merges the resulting observable sequences into one observable sequence.
    func flatMap<InnerP: Publisher>(_ selector: @escaping (P.Output) ->  Solo<InnerP>) -> Solo<Publishers.FlatMap<InnerP, P>>
    where InnerP.Failure == P.Failure {
        let unwrapped: (P.Output) -> InnerP = { selector($0).publisher }
        return Solo<Publishers.FlatMap<InnerP, P>>(publisher: publisher.flatMap(unwrapped)) }

    /// Converts any failure from the upstream publisher into a new error.
    func mapError<NewFailure: Error>(_ transform: @escaping (P.Failure) -> NewFailure) -> Solo<Publishers.MapError<P, NewFailure>> {
        let mapped: Publishers.MapError<P, NewFailure> = publisher.mapError(transform)
        return Solo<Publishers.MapError<P, NewFailure>>(publisher: mapped)
    }
}

public extension Solo {
    /// Used on Solos with Failure types of SoloError.
    /// If a Solo has a SoloError Failure type, but you are confident that a SoloError could not occur, you may use this operator
    /// to convert the SoloError to its generic error type.
    /// If there is a SoloError present, this operator will cease program execution on DEBUG builds, or never complete the Solo on Production.
    ///
    /// Example of a situation where this might be used:
    /// ```
    /// let solo = anotherSolo
    ///   .asPublisher().share().asSolo()
    ///   .assertNoSoloError()
    /// ```
    /// In this case, the developer knows that the Publisher `share()` operator would not introduce any premature completions or
    /// extra elements on the stream, so they can use assertNoSoloError() to preserve the original Failure type.
    func assertNoSoloError<E: Error>() -> AnySolo<P.Output, E> where P.Failure == SoloError<E> {
        let downstream = publisher.tryCatch { soloError -> Empty<P.Output, E> in
            switch soloError {
            case .error(let error): throw error
            case .noElements, .tooManyElements:
                soloAssertionFailure("assertNoSoloError failed - \(soloError)")
                return Empty(completeImmediately: false)
            }
        }.mapError { $0 as! E }
        return AnySolo<P.Output, E>(publisher: downstream.eraseToAnyPublisher())
    }
}
