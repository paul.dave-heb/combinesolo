//
//  Publisher+AsSolo.swift
//  
//
//  Created by Paul,David on 2/10/21.
//

import Combine

public enum SoloError<E: Swift.Error>: Swift.Error {
    case noElements
    case tooManyElements
    case error(E)
}

extension SoloError: Equatable where E: Equatable {}

public extension Publisher {
    /// Converts a Publisher to a Solo.
    func asSolo() -> Solo<Publishers.AsSolo<Self>> {
        Solo(publisher: Publishers.AsSolo(upstream: self))
    }
}

extension Publishers {
    public struct AsSolo<Upstream: Publisher>: Publisher {
        public typealias Output = Upstream.Output

        public typealias Failure = SoloError<Upstream.Failure>

        public let upstream: Upstream

        public init(upstream: Upstream) {
            self.upstream = upstream
        }

        public func receive<Downstream: Subscriber>(subscriber: Downstream)
        where Upstream.Output == Downstream.Input, Downstream.Failure == SoloError<Upstream.Failure>
        {
            upstream.subscribe(Inner(downstream: subscriber))
        }
    }
}

extension Publishers.AsSolo {
    private final class Inner<Downstream: Subscriber>: Subscriber, CustomStringConvertible,
                                                  CustomReflectable,
                                                  CustomPlaygroundDisplayConvertible
    where Upstream.Output == Downstream.Input, Downstream.Failure == SoloError<Upstream.Failure> {
        typealias Input = Upstream.Output
        typealias Failure = Upstream.Failure

        private var event: Input?

        private let downstream: Downstream

        let combineIdentifier = CombineIdentifier()

        fileprivate init(downstream: Downstream) {
            self.downstream = downstream
        }

        func receive(subscription: Subscription) {
            downstream.receive(subscription: subscription)
        }

        func receive(_ input: Input) -> Subscribers.Demand {
            guard event == nil else {
                downstream.receive(completion: .failure(.tooManyElements))
                return .none
            }

            event = input
            return .none
        }

        func receive(completion: Subscribers.Completion<Failure>) {
            switch completion {
            case .finished:
                if let event = self.event {
                    _ = downstream.receive(event)
                    downstream.receive(completion: .finished)
                }
                else {
                    downstream.receive(completion: .failure(.noElements))
                }
            case .failure(let error):
                downstream.receive(completion: .failure(.error(error)))
            }
        }

        var description: String { return "Single" }

        var customMirror: Mirror {
            return Mirror(self, children: EmptyCollection())
        }

        var playgroundDescription: Any { description }
    }
}
