//
//  SoloAssertionFailure.swift
//  
//
//  Created by Paul,David on 2/11/21.
//

import Foundation

internal var soloAssertionFailures = [String]()

internal func soloAssertionFailure(_ message: @autoclosure () -> String = String(), file: StaticString = #file, line: UInt = #line) {
    if NSClassFromString("XCTestCase") != nil {
        soloAssertionFailures.append(message())
        return
    }
    assertionFailure(message(), file: file, line: line)
}
