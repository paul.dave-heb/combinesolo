# SoloPublisher

A `Solo` is a Combine Publisher that will always complete with exactly one element.

`Solo` is modeled after RxSwift's `Single` trait: https://github.com/ReactiveX/RxSwift/blob/main/Documentation/Traits.md

Create a `Solo` with `AnySolo.create()`,  `Solo.just()`, `Solo.never()`, or `Solo.fail`.

Convert an existing Publisher to a `Solo` with `asSolo()`.

Convert a `Solo` to a Publisher with `asPublisher()`

`Solo`s support a subset of Publisher operators, like `map`, `flatMap`, `mapError`.  If you need a `Solo` operator that is not yet supported, please add it to this library!
